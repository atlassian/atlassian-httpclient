package com.atlassian.httpclient.api.factory;

/**
 * Contains certificate trust strategy options when configuring the HTTP client.
 * <p>
 * A strategy can determine the trustworthiness of certificates without consulting
 * the trust manager configured in the actual SSL context.
 *
 * @since 3.1.0, 4.1.0
 */
public enum CertificateTrustStrategy {

    /**
     * No trust strategy. This means full certificate verification is required.
     */
    NONE,
    /**
     * A trust strategy that accepts self-signed certificates as trusted.
     */
    SELF_SIGNED,
    /**
     * A trust strategy that accepts all certificates as trusted. This means full
     * certificate verification is skipped.
     */
    ALL
}

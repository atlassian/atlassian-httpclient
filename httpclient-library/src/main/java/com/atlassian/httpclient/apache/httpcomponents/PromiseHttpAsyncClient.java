package com.atlassian.httpclient.apache.httpcomponents;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.protocol.HttpContext;
import io.atlassian.util.concurrent.Promise;

interface PromiseHttpAsyncClient {
    Promise<HttpResponse> execute(HttpUriRequest request, HttpContext context);
}

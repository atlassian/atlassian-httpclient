package com.atlassian.httpclient.apache.httpcomponents;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HeadersTest {

    @Test
    public void testGetHeaderCaseInsensitive() {
        Headers headers = new Headers.Builder().setHeader("ABC", "EFG").build();

        assertEquals("EFG", headers.getHeader("ABC"));
        assertEquals("EFG", headers.getHeader("abc"));
        assertEquals("EFG", headers.getHeader("aBc"));
    }

    @Test
    public void testGetHeadersCaseInsensitive() {
        Headers headers = new Headers.Builder()
                .setHeaders(new HashMap<String, String>() {
                    {
                        put("HEADER", "value");
                        put("x-OAUTH-scopes", "repository");
                    }
                })
                .build();

        Map<String, String> headersMap = headers.getHeaders();
        assertEquals("value", headersMap.get("header"));
        assertEquals("value", headersMap.get("HEAder"));
        assertEquals("repository", headersMap.get("x-oauth-scopes"));
        assertEquals("repository", headersMap.get("X-OAuth-Scopes"));
    }
}

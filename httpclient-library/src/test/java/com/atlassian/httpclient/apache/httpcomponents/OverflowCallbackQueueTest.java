package com.atlassian.httpclient.apache.httpcomponents;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.sun.net.httpserver.HttpServer;

import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.httpclient.api.Response;
import com.atlassian.httpclient.api.ResponsePromise;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * This test exists to validate the fix for https://jira.atlassian.com/browse/BSERV-14287. The bug fixed was that the
 * callback threadpool used by SettableFuturePromiseHttpPromiseAsyncClient didn't throw RejectedExecutionException when
 * the queue was full and as a result the promise returned from SettableFuturePromiseHttpPromiseAsyncClient.execute()
 * would never be completed if the queue was overflowed.
 */
public class OverflowCallbackQueueTest {

    private HttpServer server;
    private HttpClient client;
    private int port;

    @Before
    public void setUp() {
        server = createOnFreePort();
        port = server.getAddress().getPort();
        server.setExecutor(Executors.newFixedThreadPool(8));
        server.createContext("/", x -> {
            String resp = "OK";
            x.sendResponseHeaders(200, resp.length());
            x.getResponseBody().write(resp.getBytes());
            x.getResponseBody().close();
        });
        server.start();
        client = new ApacheAsyncHttpClient<Void>("test");
    }

    @After
    public void tearDown() {
        server.stop(0);
    }

    @Test(timeout = 30000L)
    public void testOverflowCallbackQueue() throws InterruptedException {
        final int REQUEST_COUNT = 1000;
        List<ResponsePromise> requests =
                IntStream.range(0, REQUEST_COUNT).mapToObj(this::request).collect(Collectors.toList());

        int completed = 0;
        int overflow = 0;
        for (ResponsePromise r : requests) {
            try {
                Response response = r.get();
                assertEquals(200, response.getStatusCode());
                completed++;
            } catch (ExecutionException e) {
                if (e.getCause().getMessage().startsWith("Callback thread pool overflow")) {
                    // This is expected, some fraction can (and should) fail due to callback queue overflow
                    overflow++;
                } else {
                    fail("Unexpected ExecutionException:" + e.getMessage());
                }
            }
        }

        assertEquals(REQUEST_COUNT, completed + overflow);
        // If zero overflows occur it might be necessary to increase the sleep time below in request(int)
        assertTrue("No queue overflows occurred; test ineffective at validating desired behaviour", overflow > 0);
    }

    public ResponsePromise request(int i) {
        ResponsePromise p = client.newRequest(serverUrl(port)).get();
        p.done(a -> {
            try {
                // Sleep long enough to ensure the httpclient-callback threadpool and queue overflow
                Thread.sleep(100);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        return p;
    }

    private static HttpServer createOnFreePort() {
        try {
            return HttpServer.create(new InetSocketAddress(0), 0);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String serverUrl(int port) {
        return String.format("http://localhost:%d/", port);
    }
}

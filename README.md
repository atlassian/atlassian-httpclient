# Atlassian HttpClient

## Atlassian Developer?

### Committing Guidelines

Please see [the following guide](https://extranet.atlassian.com/x/Uouvdg) for committing to this module.

### Builds

The Bamboo builds for this project are [HERE](https://ecosystem-bamboo.internal.atlassian.com/browse/AHCP).

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/HC)


## Code Quality

This repository enforces the Palantir code style using the Spotless Maven plugin. Any violations of the code style will result in a build failure.

### Guidelines:
- **Formatting Code:** Before raising a pull request, run `mvn spotless:apply` to format your code.
- **Configuring IntelliJ:** Follow [this detailed guide](https://hello.atlassian.net/wiki/spaces/AB/pages/4249202122/Spotless+configuration+in+IntelliJ+IDE) to integrate Spotless into your IntelliJ IDE.

